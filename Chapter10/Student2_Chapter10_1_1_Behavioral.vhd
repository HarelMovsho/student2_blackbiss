--______ _    _    _____ _____  ___  ___  ___
--|  ___| |  | |  |_   _|  ___|/ _ \ |  \/  |
--| |_  | |  | |    | | | |__ / /_\ \| .  . |
--|  _| | |/\| |    | | |  __||  _  || |\/| |
--| |   \  /\  /    | | | |___| | | || |  | |
--\_|    \/  \/     \_/ \____/\_| |_/\_|  |_/
--
--------------------------------------------------------------------------------
-- Projetc: ‏‏10.1.Behavioral
-- File: ‏‏10.1.Behavioral.vhd
--------------------------------------------------------------------------------
-- Description: the answer to the question.
--
--------------------------------------------------------------------------------
--       Version:       Author:          Date:       Change:
--            0.0       Garshon          1/1/2020    Initial
--            0.1       Student2         10/4/2020
--
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity Behavioral_Answer is
   port(
   	-------------------
   	-- General Ports --
   	-------------------
   	clk	: in std_logic;
   	rst : in std_logic;

   	-----------------
   	-- Input ports --
   	-----------------
   	A, B, C, D, E : in bit;

   	------------------
   	-- Output ports --
   	------------------
   	I : out bit

    );
End entity Behavioral_Answer;


Architecture Behavioral_Answer_ARCH of Behavioral_Answer is
	signal F : bit;
	signal G : bit;
	signal N : bit;
Begin

process(A, B, C, D, E)
Begin
   if (A = '0' and B = '1' and C = '1') then
      F <= '1';
   else
      F <= '0';
   End if;

   if (D = '1' and E = '0') then
      G <= '1';
   else
      G <= '0';
   End if;
End process;
process(F, G)
Begin
   if (F = '1' xor G = '1') then
      I <= '0';
   else
      I <= '1';
   End if;
End process;



End Architecture Behavioral_Answer_ARCH;