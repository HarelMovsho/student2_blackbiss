--______ _    _    _____ _____  ___  ___  ___
--|  ___| |  | |  |_   _|  ___|/ _ \ |  \/  |
--| |_  | |  | |    | | | |__ / /_\ \| .  . |
--|  _| | |/\| |    | | |  __||  _  || |\/| |
--| |   \  /\  /    | | | |___| | | || |  | |
--\_|    \/  \/     \_/ \____/\_| |_/\_|  |_/
--
--------------------------------------------------------------------------------
-- Projetc: ‏‏10.7
-- File: ‏‏10.7.vhd
--------------------------------------------------------------------------------
-- Description: the answer to the question.
--
--------------------------------------------------------------------------------
--       Version:       Author:          Date:       Change:
--            0.1       Student2         12/4/2020
--
--
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity Sum is
   port(
   	-------------------
   	-- General Ports --
   	-------------------
   	clk	: in std_logic;
   	rst : in std_logic;

   	-----------------
   	-- Input ports --
   	-----------------
      A : in std_logic_vector(15 downto 0);
      B : in std_logic_vector(15 downto 0);
      C : in std_logic_vector(15 downto 0);
      D : in std_logic_vector(15 downto 0);

   	------------------
   	-- Output ports --
   	------------------
   	Z : out std_logic_vector(15 downto 0)
    
    );
End entity Sum;


Architecture Sum_ARCH of Sum is
   signal sum : std_logic_vector(17 downto 0);
Begin
   sum <= std_logic_vector('0'&'0'&(unsigned(A) + unsigned(B)+ unsigned(C) + unsigned(D)));
   Z <= std_logic_vector(unsigned(sum(17 downto 2)) + (sum(1) & ""));

End Architecture Sum_ARCH;