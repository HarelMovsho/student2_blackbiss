--______ _    _    _____ _____  ___  ___  ___
--|  ___| |  | |  |_   _|  ___|/ _ \ |  \/  |
--| |_  | |  | |    | | | |__ / /_\ \| .  . |
--|  _| | |/\| |    | | |  __||  _  || |\/| |
--| |   \  /\  /    | | | |___| | | || |  | |
--\_|    \/  \/     \_/ \____/\_| |_/\_|  |_/
--
--------------------------------------------------------------------------------
-- Projetc: ‏‏10.5
-- File: ‏‏10.5.vhd
--------------------------------------------------------------------------------
-- Description: the answer to the question.
--
--------------------------------------------------------------------------------
--       Version:       Author:          Date:       Change:
--            0.1       Student2         12/4/2020
--
--
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
library BITLIB;
use BITLIB.bit_pack.all; 

Entity Full_Adder is
   port(
   	-------------------
   	-- General Ports --
   	-------------------
   	clk	: in std_logic;
   	rst : in std_logic;

   	-----------------
   	-- Input ports --
   	-----------------
   	A : in bit;
   	B : in bit;
   	C : in bit;

   	------------------
   	-- Output ports --
   	------------------
   	S : out bit_vector(0 to 1)
    
    );
End entity Full_Adder;

Architecture Full_Adder_ARCH of Full_Adder is
 type ROM8X2 is array (0 to 7) of bit_vector(0 to 1);
 constant Full_Adder_Table : ROM8X2 := ("00", "01", "01", "10", "01", "10", "10", "11");
 signal Adder_Input : Integer range 0 to 7;
Begin
   Adder_Input <= vec2int(A&B&C);
   S <= Full_Adder_Table(Adder_Input);



End Architecture Full_Adder_ARCH;