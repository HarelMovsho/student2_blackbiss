--______ _    _    _____ _____  ___  ___  ___
--|  ___| |  | |  |_   _|  ___|/ _ \ |  \/  |
--| |_  | |  | |    | | | |__ / /_\ \| .  . |
--|  _| | |/\| |    | | |  __||  _  || |\/| |
--| |   \  /\  /    | | | |___| | | || |  | |
--\_|    \/  \/     \_/ \____/\_| |_/\_|  |_/
--
--------------------------------------------------------------------------------
-- Projetc: ‏‏10.1.Structural
-- File: ‏‏10.1.Structural.vhd
--------------------------------------------------------------------------------
-- Description: the answer to the question.
--
--------------------------------------------------------------------------------
--       Version:       Author:          Date:       Change:
--            0.1       Student2         10/4/2020
--
--
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity Structural_Answer is
   port(
   	-------------------
   	-- General Ports --
   	-------------------
   	clk	: in bit;
   	rst : in bit;

   	-----------------
   	-- Input ports --
   	-----------------
   	A, B, C, D, E : in bit;

   	------------------
   	-- Output ports --
   	------------------
   	I : out bit

    );
End entity Structural_Answer;


Architecture Structural_Answer_ARCH of Structural_Answer is
   ------------------
   -- Components --
   ------------------
   component and3_gate
   port( a, b, c: in bit;
   	d : out bit);
   end component;


   component and2_gate
   port( a, b : in bit;
   	c : out bit);
   end component;


   component xor2_gate
   port( a, b : in bit;
   	c : out bit);
   end component;


   component not_gate
   port( a : in bit;
   	b : out bit);
   end component;

   ------------------
   -- Signals --
   ------------------
	signal F : bit;
	signal G : bit;
	signal N : bit;
   
Begin
	and1 : and3_gate port map(A, B, C ,F);
	and2 : and2_gate port map(D, E, G);
	xor1 : xor2_gate port map(F, G, N);
	not1 : not_gate port map(N, I);

End Architecture Structural_Answer_ARCH;