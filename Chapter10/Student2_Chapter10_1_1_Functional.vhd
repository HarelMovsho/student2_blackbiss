--______ _    _    _____ _____  ___  ___  ___
--|  ___| |  | |  |_   _|  ___|/ _ \ |  \/  |
--| |_  | |  | |    | | | |__ / /_\ \| .  . |
--|  _| | |/\| |    | | |  __||  _  || |\/| |
--| |   \  /\  /    | | | |___| | | || |  | |
--\_|    \/  \/     \_/ \____/\_| |_/\_|  |_/
--
--------------------------------------------------------------------------------
-- Projetc: ‏‏10.1.Functional
-- File: ‏‏10.1.Functional.vhd
--------------------------------------------------------------------------------
-- Description: the answer to the question.
--
--------------------------------------------------------------------------------
--		Version:			Author:				Date:		Change:
--         0.0				Garshon				1/1/2020	Initial
--         0.1          	Student2          	10/4/2020
--
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity Functional_Answer is
   port(
   	-------------------
   	-- General Ports --
   	-------------------
   	clk	: in std_logic;
   	rst : in std_logic;

   	-----------------
   	-- Input ports --
   	-----------------
   	A, B, C, D, E : in bit;

   	------------------
   	-- Output ports --
   	------------------
   	I : out bit

    );
End entity Functional_Answer;


Architecture Functional_Answer_ARCH of Functional_Answer is
	signal F : bit;
	signal G : bit;
	signal N : bit;
Begin
	F <= not A and B and C;
	G <= D and not E;
	N <= F xor G;
	I <= not N;

End Architecture Functional_Answer_ARCH;