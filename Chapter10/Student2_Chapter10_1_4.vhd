--______ _    _    _____ _____  ___  ___  ___
--|  ___| |  | |  |_   _|  ___|/ _ \ |  \/  |
--| |_  | |  | |    | | | |__ / /_\ \| .  . |
--|  _| | |/\| |    | | |  __||  _  || |\/| |
--| |   \  /\  /    | | | |___| | | || |  | |
--\_|    \/  \/     \_/ \____/\_| |_/\_|  |_/
--
--------------------------------------------------------------------------------
-- Projetc: ‏‏10.4
-- File: ‏‏10.4.vhd
--------------------------------------------------------------------------------
-- Description: the answer to the question.
--
--------------------------------------------------------------------------------
--       Version:       Author:          Date:       Change:
--            0.1       Student2         10/4/2020
--
--
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
-- TODO: import lib here, if needed

Entity Simple_MUX is
   port(
   	-------------------
   	-- General Ports --
   	-------------------
   	clk	: in std_logic;
   	rst : in std_logic;

   	-----------------
   	-- Input ports --
   	-----------------
   	A : in bit;
   	X : in bit_vector(3 downto 0);
   	Y : in bit_vector(3 downto 0);

   	------------------
   	-- Output ports --
   	------------------
   	Z : out bit_vector(3 downto 0)
    
    );
End entity Simple_MUX;


Architecture Simple_MUX_ARCH of Simple_MUX is
Begin
	with A select
		Z <= X when '0',
			Y when '1';


End Architecture Simple_MUX_ARCH;