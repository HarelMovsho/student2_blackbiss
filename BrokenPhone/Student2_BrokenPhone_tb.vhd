LIBRARY ieee ;
LIBRARY std ;
USE ieee.NUMERIC_STD.all ;
USE ieee.std_logic_1164.all ;
USE ieee.std_logic_textio.all ;
USE ieee.std_logic_unsigned.all ;
USE std.textio.all ;
ENTITY \Student2_BrokenPhone_tb.vhd\ IS
END ;

ARCHITECTURE \Student2_BrokenPhone_tb.vhd_arch\ OF \Student2_BrokenPhone_tb.vhd\ IS
	SIGNAL Boom_out : BIT ;
	SIGNAL I0       : BIT ;
	SIGNAL inc_call : BIT ;
	SIGNAL I5       : BIT ;
	SIGNAL I1       : BIT ;
	SIGNAL B0       : BIT ;
	SIGNAL clk      : STD_LOGIC ;
	SIGNAL I2       : BIT ;
	SIGNAL B1       : BIT ;
	COMPONENT Broken_Phone
		PORT (
			Boom_out : out BIT ;
			I0       : in  BIT ;
			inc_call : in  BIT ;
			I5       : in  BIT ;
			I1       : in  BIT ;
			B0       : in  BIT ;
			clk      : in  STD_LOGIC ;
			I2       : in  BIT ;
			B1       : in  BIT );
	END COMPONENT ;
BEGIN
	DUT : Broken_Phone
		PORT MAP (
			Boom_out => Boom_out ,
			I0       => I0 ,
			inc_call => inc_call ,
			I5       => I5 ,
			I1       => I1 ,
			B0       => B0 ,
			clk      => clk ,
			I2       => I2 ,
			B1       => B1 ) ;


	Process
	Begin
		clk <= '0' ;
		wait for 3 ns ;
		for Z in 1 to 25
			loop
				clk <= '1' ;
				wait for 2 ns ;
				clk <= '0' ;
				wait for 3 ns ;
			end loop;
			clk <= '1' ;
			wait for 2 ns ;
			wait;
		End Process;


		Process
		Begin
			b1 <= '0' ;
			b0 <= '0' ;
			wait for 10 ns ;
			b0 <= '1' ;
			wait for 10 ns ;
			b0 <= '0' ;
			b1 <= '1' ;
			wait for 10 ns ;
			b1 <= '0' ;
			wait;
		End Process;


		Process
		Begin
			inc_call <= '0' ;
			i0       <= '0' ;
			i1       <= '0' ;
			i2       <= '0' ;
			i5       <= '0' ;
			wait for 50 ns ;
			inc_call <= '1' ;
			i0       <= '1' ;
			wait for 5 ns ;
			i0 <= '0' ;
			i5 <= '1' ;
			wait for 5 ns ;
			i5 <= '0' ;
			i0 <= '1' ;
			wait for 15 ns ;
			i0 <= '0' ;
			i1 <= '1' ;
			wait for 15 ns ;
			i1 <= '0' ;
			i2 <= '1' ;
			wait for 10 ns ;
			i2       <= '0' ;
			inc_call <= '0' ;
			wait;
		End Process;
	End;
