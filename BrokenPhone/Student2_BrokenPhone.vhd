--______ _    _    _____ _____  ___  ___  ___
--|  ___| |  | |  |_   _|  ___|/ _ \ |  \/  |
--| |_  | |  | |    | | | |__ / /_\ \| .  . |
--|  _| | |/\| |    | | |  __||  _  || |\/| |
--| |   \  /\  /    | | | |___| | | || |  | |
--\_|    \/  \/     \_/ \____/\_| |_/\_|  |_/
--
--------------------------------------------------------------------------------
-- Projetc: Student2_BrokenPhone
-- File: Student2_BrokenPhone.vhd
--------------------------------------------------------------------------------
-- Description: The code that will make your head explode!
--
--------------------------------------------------------------------------------
--		Version:			Author:				Date:		Change:
--         0.0				Garshon				1/1/2020	Initial
--
--
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
-- TODO: import lib here, if needed

Entity Broken_Phone is
	port(
		clk : in std_logic;
		-----------------
		-- Input ports --
		-----------------
		B0, B1         : in bit;
		inc_call       : in bit;
		I0, I1, I2, I5 : in bit;
		------------------
		-- Output ports --
		------------------
		Boom_out : out bit

	);
End entity Broken_Phone;

Architecture Broken_Phone_ARCH of Broken_Phone is
	signal state : integer range 0 to 2;
	signal i     : integer range 0 to 9;
Begin
	process(B0, B1)
	Begin
		case state is
			when 0 =>
				if B0 = '1' then state <= 1; end if;
			when 1 =>
				if B1 = '1' then state <= 2; end if;
			when 2 =>
		end case;
	End process;
	process(clk)
	Begin
		if state = 2 and inc_call = '1' then
			if clk'event and clk = '1' then
				case i is
					when 0 => if I0 = '1' then i <= i + 1; else i <= 0; end if;
					when 1 => if I5 = '1' then i <= i + 1; else i <= 0; end if;
					when 2 => if I0 = '1' then i <= i + 1; else i <= 0; end if;
					when 3 => if I0 = '1' then i <= i + 1; else i <= 0; end if;
					when 4 => if I0 = '1' then i <= i + 1; else i <= 0; end if;
					when 5 => if I1 = '1' then i <= i + 1; else i <= 0; end if;
					when 6 => if I1 = '1' then i <= i + 1; else i <= 0; end if;
					when 7 => if I1 = '1' then i <= i + 1; else i <= 0; end if;
					when 8 => if I2 = '1' then i <= i + 1; else i <= 0; end if;
					when 9 => if I2 = '1' then Boom_out <= '1'; else i <= 0; end if;
				end case;
			end if;
		end if;
	end process;


End Architecture Broken_Phone_ARCH;