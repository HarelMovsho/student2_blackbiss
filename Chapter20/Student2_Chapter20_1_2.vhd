--______ _    _    _____ _____  ___  ___  ___
--|  ___| |  | |  |_   _|  ___|/ _ \ |  \/  |
--| |_  | |  | |    | | | |__ / /_\ \| .  . |
--|  _| | |/\| |    | | |  __||  _  || |\/| |
--| |   \  /\  /    | | | |___| | | || |  | |
--\_|    \/  \/     \_/ \____/\_| |_/\_|  |_/
--
--------------------------------------------------------------------------------
-- Projetc: ‏‏Student2_Chapter20_1_2
-- File: ‏‏Student2_Chapter20_1_2.vhd
--------------------------------------------------------------------------------
-- Description: the answer to the question.
--
--------------------------------------------------------------------------------
--       Version:       Author:          Date:       Change:
--            0.1       Student2         14/4/2020
--
--
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
-- TODO: import lib here, if needed

Entity Twos_Complementer is
	port(
		-------------------
		-- General Ports --
		-------------------
		clk : in std_logic;

		-----------------
		-- Input ports --
		-----------------
		N : in std_logic;
		I : in std_logic_vector(15 downto 0);

		------------------
		-- Output ports --
		------------------
		O : out std_logic_vector(15 downto 0)

	);
End entity Twos_Complementer;

Architecture Twos_Complementer_ARCH of Twos_Complementer is
	signal K     : integer range 0 to 15;
	signal state : integer range 0 to 2;
	signal reg   : std_logic_vector(15 downto 0);
Begin
	process(clk, N)
	Begin
		if (clk'event and clk = '1') then
			if N = '0' then
				K     <= 0;
				state <= 0;
			elsif state = 2 then
				K <= 0;
				O <= reg;
			elsif N = '1' then
				if(K < 15) then K <= K + 1;
				else state <= 2;
				End if;
				if(state = 0) then
					if reg(0) = '1' then state <= 1; end if;
					if K = 0 then reg <= I(0) & I(15 downto 1);
					else reg <= reg(0) & reg(15 downto 1);
					end if;
				else
					reg <= not reg(0) & reg(15 downto 1);
				End if;
			End if;
		End if;
	End process;


End Architecture Twos_Complementer_ARCH;