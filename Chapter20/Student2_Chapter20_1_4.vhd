library IEEE;
use IEEE.STD_LOGIC_1164. ALL;
use IEEE.STD_LOGIC_ARITH. ALL;
use IEEE.STD_LOGIC_UNSIGNED. ALL;

entity testconverter is
end testconverter;

architecture test1 of testconverter is
	component SM17_2
		port (
			X, Clk : in  bit;
			Z      : out bit
		);
	end component;
	constant N     : integer := 39;
	signal X       : bit;
	signal Z       : bit;
	signal Clk     : bit;
	signal BCD     : bit_vector(39 downto 0) := "0000000100100011010001010110011110001001";
	signal Excess3 : bit_vector(39 downto 0) := "0011010001010110011110001001101010111100";
	signal err     : bit;
begin
		converter1 : SM17_2 port map(X, Clk, Z);
	Clk <= not Clk after 10 ns;
	process
	begin
		for i in 0 to N loop
			X <= BCD(i);
			wait until Clk = '1' and Clk'event;
			if Z = not Excess3(i) then err <= '1'; End if;
		end loop;
	end process;
end test1;