LIBRARY ieee ;
LIBRARY std ;
USE ieee.NUMERIC_STD.all ;
USE ieee.std_logic_1164.all ;
USE ieee.std_logic_textio.all ;
USE ieee.std_logic_unsigned.all ;
USE std.textio.all ;
ENTITY \Student2_Chapter20_1_2_tb.vhd\ IS
END ;

ARCHITECTURE \Student2_Chapter20_1_2_tb.vhd_arch\ OF \Student2_Chapter20_1_2_tb.vhd\ IS
	SIGNAL I   : std_logic_vector (15 downto 0) ;
	SIGNAL N   : STD_LOGIC ;
	SIGNAL clk : STD_LOGIC ;
	SIGNAL O   : std_logic_vector (15 downto 0) ;
	COMPONENT Twos_Complementer
		PORT (
			I   : in  std_logic_vector (15 downto 0) ;
			N   : in  STD_LOGIC ;
			clk : in  STD_LOGIC ;
			O   : out std_logic_vector (15 downto 0) );
	END COMPONENT ;
BEGIN
	DUT : Twos_Complementer
		PORT MAP (
			I   => I ,
			N   => N ,
			clk => clk ,
			O   => O ) ;



	Process
	Begin
		clk <= '0' ;
		wait for 5 ns ;
		for Z in 1 to 19
			loop
				clk <= '1' ;
				wait for 5 ns ;
				clk <= '0' ;
				wait for 5 ns ;
			end loop;
			clk <= '1' ;
			wait for 5 ns ;
			wait;
		End Process;

		Process
		Begin
			n <= '0' ;
			i <= "0110100100000000" ;
			wait for 10 ns ;
			n <= '1' ;
			wait for 190 ns ;
			wait;
		End Process;
	END;
