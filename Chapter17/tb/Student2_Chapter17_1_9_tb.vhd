LIBRARY ieee  ; 
LIBRARY std  ; 
USE ieee.NUMERIC_STD.all  ; 
USE ieee.std_logic_1164.all  ; 
USE ieee.std_logic_textio.all  ; 
USE ieee.std_logic_unsigned.all  ; 
USE std.textio.all  ; 
ENTITY \Student2_Chapter17_1_9_tb.vhd\  IS 
END ; 
 
ARCHITECTURE \Student2_Chapter17_1_9_tb.vhd_arch\   OF \Student2_Chapter17_1_9_tb.vhd\   IS
  SIGNAL Q   :  STD_LOGIC  ; 
  SIGNAL R   :  STD_LOGIC  ; 
  SIGNAL clk   :  STD_LOGIC  ; 
  SIGNAL nQ   :  STD_LOGIC  ; 
  SIGNAL S   :  STD_LOGIC  ; 
  COMPONENT SR_FlipFlop  
    PORT ( 
      Q  : out STD_LOGIC ; 
      R  : in STD_LOGIC ; 
      clk  : in STD_LOGIC ; 
      nQ  : out STD_LOGIC ; 
      S  : in STD_LOGIC ); 
  END COMPONENT ; 
BEGIN
  DUT  : SR_FlipFlop  
    PORT MAP ( 
      Q   => Q  ,
      R   => R  ,
      clk   => clk  ,
      nQ   => nQ  ,
      S   => S   ) ; 


  Process
	Begin
	 clk  <= '0'  ;
	wait for 5 ns ;
	for Z in 1 to 19
	loop
	    clk  <= '1'  ;
	   wait for 5 ns ;
	    clk  <= '0'  ;
	   wait for 5 ns ;
	end  loop;
	 clk  <= '1'  ;
	wait for 5 ns ;
	wait;
 End Process;


  Process
	Begin
	 s  <= '0'  ;
	wait for 100 ns ;
	 s  <= '1'  ;
	wait for 100 ns ;
	wait;
 End Process;


  Process
	Begin
	 r  <= '0'  ;
	wait for 50 ns ;
	 r  <= '1'  ;
	wait for 100 ns ;
	 r  <= '0'  ;
	wait for 50 ns ;
	wait;
 End Process;
END;
