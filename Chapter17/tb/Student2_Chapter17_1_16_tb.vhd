LIBRARY ieee  ; 
LIBRARY std  ; 
USE ieee.NUMERIC_STD.all  ; 
USE ieee.std_logic_1164.all  ; 
USE ieee.std_logic_textio.all  ; 
USE ieee.std_logic_unsigned.all  ; 
USE std.textio.all  ; 
ENTITY \Student2_Chapter17_1_16_tb.vhd\  IS 
END ; 
 
ARCHITECTURE \Student2_Chapter17_1_16_tb.vhd_arch\   OF \Student2_Chapter17_1_16_tb.vhd\   IS
  SIGNAL I   :  std_logic_vector (3 downto 0)  ; 
  SIGNAL rst   :  STD_LOGIC  ; 
  SIGNAL clk   :  STD_LOGIC  ; 
  SIGNAL O   :  std_logic_vector (6 downto 0)  ; 
  COMPONENT BCD_to_7Seg  
    PORT ( 
      I  : in std_logic_vector (3 downto 0) ; 
      rst  : in STD_LOGIC ; 
      clk  : in STD_LOGIC ; 
      O  : out std_logic_vector (6 downto 0) ); 
  END COMPONENT ; 
BEGIN
  DUT  : BCD_to_7Seg  
    PORT MAP ( 
      I   => I  ,
      rst   => rst  ,
      clk   => clk  ,
      O   => O   ) ; 


  Process
	Begin
	 i  <= "0000"  ;
	wait for 20 ns ;
	 i  <= "0001"  ;
	 wait for 20 ns ;
	 i  <= "0010"  ; 
	 wait for 20 ns ;
	 i  <= "0011"  ;  
	 wait for 20 ns ;
	 i  <= "0100"  ;  
	 wait for 20 ns ;
	 i  <= "0101"  ;
	 wait for 20 ns ;
	 i  <= "0110"  ;  
	 wait for 20 ns ;
	 i  <= "0111"  ;
	 wait for 20 ns ;
	 i  <= "1000"  ;
	 wait for 20 ns ;
	 i  <= "1001"  ;
	wait;
 End Process;
END;
