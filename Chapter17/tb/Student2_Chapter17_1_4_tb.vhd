LIBRARY ieee  ; 
LIBRARY std  ; 
USE ieee.NUMERIC_STD.all  ; 
USE ieee.std_logic_1164.all  ; 
USE ieee.std_logic_textio.all  ; 
USE ieee.std_logic_unsigned.all  ; 
USE std.textio.all  ; 
ENTITY \Student2_Chapter17_1_4.tb.vhd\  IS 
END ; 
 
ARCHITECTURE \Student2_Chapter17_1_4.tb.vhd_arch\   OF \Student2_Chapter17_1_4.tb.vhd\   IS
  SIGNAL D   :  BIT  ; 
  SIGNAL A   :  BIT  ; 
  SIGNAL rst   :  STD_LOGIC  ; 
  SIGNAL Z   :  BIT  ; 
  SIGNAL B   :  BIT  ; 
  SIGNAL clk   :  STD_LOGIC  ; 
  SIGNAL C   :  BIT  ; 
  COMPONENT Exercise4  
    PORT ( 
      D  : in BIT ; 
      A  : in BIT ; 
      rst  : in STD_LOGIC ; 
      Z  : out BIT ; 
      B  : in BIT ; 
      clk  : in STD_LOGIC ; 
      C  : in BIT ); 
  END COMPONENT ; 
BEGIN
  DUT  : Exercise4  
    PORT MAP ( 
      D   => D  ,
      A   => A  ,
      rst   => rst  ,
      Z   => Z  ,
      B   => B  ,
      clk   => clk  ,
      C   => C   ) ; 


  Process
	Begin
	  A <= '0';
	  B <= '0';
	  C <= '1';
	  D <= '0';
	wait for 25ns;
	  A <= '1';
	wait for 25ns;
	  B <= '1';
	wait for 25ns;
	  A <= '0';
	wait for 25ns;
	wait;
 End Process;
END;