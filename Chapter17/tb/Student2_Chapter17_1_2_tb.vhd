LIBRARY ieee  ; 
LIBRARY std  ; 
USE ieee.NUMERIC_STD.all  ; 
USE ieee.std_logic_1164.all  ; 
USE ieee.std_logic_textio.all  ; 
USE ieee.std_logic_unsigned.all  ; 
USE std.textio.all  ; 
ENTITY \Student2_Chapter17_1_2_tb.vhd\  IS 
END ; 
 
ARCHITECTURE \Student2_Chapter17_1_2_tb.vhd_arch\   OF \Student2_Chapter17_1_2_tb.vhd\   IS
  SIGNAL D   :  std_logic_vector (3 downto 0)  ; 
  SIGNAL Ld   :  STD_LOGIC  ; 
  SIGNAL Q   :  std_logic_vector (3 downto 0)  ; 
  SIGNAL RS   :  STD_LOGIC  ; 
  SIGNAL clr   :  STD_LOGIC  ; 
  SIGNAL clk   :  STD_LOGIC  ; 
  SIGNAL Lin   :  STD_LOGIC  ; 
  COMPONENT Right_Shift  
    PORT ( 
      D  : in std_logic_vector (3 downto 0) ; 
      Ld  : in STD_LOGIC ; 
      Q  : out std_logic_vector (3 downto 0) ; 
      RS  : in STD_LOGIC ; 
      clr  : in STD_LOGIC ; 
      clk  : in STD_LOGIC ; 
      Lin  : in STD_LOGIC ); 
  END COMPONENT ; 
BEGIN
  DUT  : Right_Shift  
    PORT MAP ( 
      D   => D  ,
      Ld   => Ld  ,
      Q   => Q  ,
      RS   => RS  ,
      clr   => clr  ,
      clk   => clk  ,
      Lin   => Lin   ) ; 



  Process
	Begin
	 clk  <= '0'  ;
	wait for 50 ns ;
	for Z in 1 to 9
	loop
	    clk  <= '1'  ;
	   wait for 50 ns ;
	    clk  <= '0'  ;
	   wait for 50 ns ;
	end  loop;
	 clk  <= '1'  ;
	wait for 50 ns ;
	wait;
 End Process;



  Process
	Begin
	 clr  <= '1'  ;
	wait for 100 ns ;
	 clr  <= '0'  ;
	wait for 900 ns ;
	wait;
 End Process;


  Process
	Begin
	 ld  <= '1'  ;
	wait for 500 ns ;
	 ld  <= '0'  ;
	wait for 500 ns ;
	wait;
 End Process;


  Process
	Begin
	 rs  <= '0'  ;
	wait for 500 ns ;
	 rs  <= '1'  ;
	wait for 500 ns ;
	wait;
 End Process;


  Process
	Begin
	 lin  <= '0'  ;
	wait for 1 us ;
	wait;
 End Process;


  Process
	Begin
	 d  <= "1001"  ;
	wait for 300 ns ;
	 d  <= "0100"  ;
	wait for 100 ns ;
	 d  <= "1111"  ;
	wait for 600 ns ;
	wait;
 End Process;
END;
