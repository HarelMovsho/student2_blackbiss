LIBRARY ieee  ; 
LIBRARY std  ; 
USE ieee.NUMERIC_STD.all  ; 
USE ieee.std_logic_1164.all  ; 
USE ieee.std_logic_textio.all  ; 
USE ieee.std_logic_unsigned.all  ; 
USE std.textio.all  ; 
ENTITY \Student2_Chapter17_1_13_tb.vhd\  IS 
END ; 
 
ARCHITECTURE \Student2_Chapter17_1_13_tb.vhd_arch\   OF \Student2_Chapter17_1_13_tb.vhd\   IS
  SIGNAL O1   :  BIT  ; 
  SIGNAL I3   :  BIT  ; 
  SIGNAL I0   :  BIT  ; 
  SIGNAL rst   :  STD_LOGIC  ; 
  SIGNAL V   :  BIT  ; 
  SIGNAL I1   :  BIT  ; 
  SIGNAL clk   :  STD_LOGIC  ; 
  SIGNAL O0   :  BIT  ; 
  SIGNAL I2   :  BIT  ; 
  COMPONENT Priority_Encoder  
    PORT ( 
      O1  : out BIT ; 
      I3  : in BIT ; 
      I0  : in BIT ; 
      rst  : in STD_LOGIC ; 
      V  : out BIT ; 
      I1  : in BIT ; 
      clk  : in STD_LOGIC ; 
      O0  : out BIT ; 
      I2  : in BIT ); 
  END COMPONENT ; 
BEGIN
  DUT  : Priority_Encoder  
    PORT MAP ( 
      O1   => O1  ,
      I3   => I3  ,
      I0   => I0  ,
      rst   => rst  ,
      V   => V  ,
      I1   => I1  ,
      clk   => clk  ,
      O0   => O0  ,
      I2   => I2   ) ; 



  Process
	Begin
	 i0  <= '0'  ;
   i1  <= '0'  ;
   i2  <= '0'  ;
   i3  <= '1'  ;
  wait for 20 ns ;
   i3  <= '0'  ;
   i2  <= '1'  ;
  wait for 20 ns ;
   i1  <= '1'  ;
   i2  <= '0'  ;
	wait for 20 ns ;
	 i0  <= '1'  ;
   i1  <= '0'  ;
	wait for 20 ns ;
   i0  <= '0'  ;  
	wait;
 End Process;
END;