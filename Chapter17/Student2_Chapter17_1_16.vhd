--______ _    _    _____ _____  ___  ___  ___
--|  ___| |  | |  |_   _|  ___|/ _ \ |  \/  |
--| |_  | |  | |    | | | |__ / /_\ \| .  . |
--|  _| | |/\| |    | | |  __||  _  || |\/| |
--| |   \  /\  /    | | | |___| | | || |  | |
--\_|    \/  \/     \_/ \____/\_| |_/\_|  |_/
--
--------------------------------------------------------------------------------
-- Projetc: ‏‏Student2_Chapter17_1_16
-- File: ‏‏Student2_Chapter17_1_16.vhd
--------------------------------------------------------------------------------
-- Description: the answer to the question.
--
--------------------------------------------------------------------------------
--       Version:       Author:          Date:       Change:
--            0.1       Student2         13/4/2020
--
--
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity BCD_to_7Seg is
   port(
   	-------------------
   	-- General Ports --
   	-------------------
   	clk	: in std_logic;
   	rst : in std_logic;

   	-----------------
   	-- Input ports --
   	-----------------
   	I : in std_logic_vector(3 downto 0);

   	------------------
   	-- Output ports --
   	------------------
   	O : out std_logic_vector(6 downto 0)
    
    );
End entity BCD_to_7Seg;

Architecture BCD_to_7Seg_ARCH of BCD_to_7Seg is
Begin
	process(I)
	Begin
		case I is
		when "0000" => O <= "1111110";
		when "0001" => O <= "0110000";
		when "0010" => O <= "1101101";
		when "0011" => O <= "1111001";
		when "0100" => O <= "0110011";
		when "0101" => O <= "1011011";
		when "0110" => O <= "1011111";
		when "0111" => O <= "1110000";
		when "1000" => O <= "1111111";
		when "1001" => O <= "1111011";
		when others => O <= "0000000";
		End case;
	End process;


End Architecture BCD_to_7Seg_ARCH;