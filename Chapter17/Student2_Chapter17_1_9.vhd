--______ _    _    _____ _____  ___  ___  ___
--|  ___| |  | |  |_   _|  ___|/ _ \ |  \/  |
--| |_  | |  | |    | | | |__ / /_\ \| .  . |
--|  _| | |/\| |    | | |  __||  _  || |\/| |
--| |   \  /\  /    | | | |___| | | || |  | |
--\_|    \/  \/     \_/ \____/\_| |_/\_|  |_/
--
--------------------------------------------------------------------------------
-- Projetc: ‏‏10.1.Structural
-- File: ‏‏10.1.Structural.vhd
--------------------------------------------------------------------------------
-- Description: the answer to the question.
--
--------------------------------------------------------------------------------
--       Version:       Author:          Date:       Change:
--            0.1       Student2         10/4/2020
--
--
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
-- TODO: import lib here, if needed

Entity SR_FlipFlop is
   port(
   	-------------------
   	-- General Ports --
   	-------------------
   	clk	: in std_logic;

   	-----------------
   	-- Input ports --
   	-----------------
   	S : in std_logic;
   	R : in std_logic;

   	------------------
   	-- Output ports --
   	------------------
   	Q : out std_logic;
   	nQ : out std_logic
    
    );
End entity SR_FlipFlop;

Architecture SR_FlipFlop_ARCH of SR_FlipFlop is
Begin
	process(clk, S, R)
	Begin
		if clk'event and clk = '1' then
			if S = '1' then
				if R = '1' then Q <= 'X'; nQ <= 'X';
				else Q <= S; nQ <= not S;
				End if;
			elsif R = '1' then Q <= S; nQ <= not S;
			End if; 
		End if;
	End process;

End Architecture SR_FlipFlop_ARCH;