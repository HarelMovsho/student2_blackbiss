--______ _    _    _____ _____  ___  ___  ___
--|  ___| |  | |  |_   _|  ___|/ _ \ |  \/  |
--| |_  | |  | |    | | | |__ / /_\ \| .  . |
--|  _| | |/\| |    | | |  __||  _  || |\/| |
--| |   \  /\  /    | | | |___| | | || |  | |
--\_|    \/  \/     \_/ \____/\_| |_/\_|  |_/
--
--------------------------------------------------------------------------------
-- Projetc: ‏‏Student2_Chapter17_1_4
-- File: ‏‏Student2_Chapter17_1_4.vhd
--------------------------------------------------------------------------------
-- Description: the answer to the question.
--
--------------------------------------------------------------------------------
--       Version:       Author:          Date:       Change:
--            0.1       Student2         10/4/2020
--
--
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity Exercise4 is
   port(
   	-------------------
   	-- General Ports --
   	-------------------
   	clk	: in std_logic;
   	rst : in std_logic;

   	-----------------
   	-- Input ports --
   	-----------------
   	A : in bit;
	B : in bit;
	C : in bit;
	D : in bit;

   	------------------
   	-- Output ports --
   	------------------
   	Z : out bit
    
    );
End entity Exercise4;

Architecture Exercise4_ARCH of Exercise4 is
Begin
	process(A, B, C, D)
	Begin
		case A is
		when '0' =>
			if B = '0' then Z <= not C or D;
			else Z <= C; end if;
		when '1' =>
			if B = '0' then Z <= not C xor D;
			else Z <= not D; end if;
		End case;
	End process;
	
End Architecture Exercise4_ARCH;