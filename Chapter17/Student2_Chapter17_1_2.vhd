--______ _    _    _____ _____  ___  ___  ___
--|  ___| |  | |  |_   _|  ___|/ _ \ |  \/  |
--| |_  | |  | |    | | | |__ / /_\ \| .  . |
--|  _| | |/\| |    | | |  __||  _  || |\/| |
--| |   \  /\  /    | | | |___| | | || |  | |
--\_|    \/  \/     \_/ \____/\_| |_/\_|  |_/
--
--------------------------------------------------------------------------------
-- Projetc: ‏‏Student2_Chapter17_1_2
-- File: ‏‏Student2_Chapter17_1_2.vhd
--------------------------------------------------------------------------------
-- Description: the answer to the question.
--
--------------------------------------------------------------------------------
--       Version:       Author:          Date:       Change:
--            0.1       Student2         13/4/2020
--
--
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

Entity Right_Shift is
   port(
   	-------------------
   	-- General Ports --
   	-------------------
   	clk	: in std_logic;
   	clr : in std_logic;
   	Ld : in std_logic;
   	RS : in std_logic;

   	-----------------
   	-- Input ports --
   	-----------------
	  Lin : in std_logic;
	  D : in std_logic_vector(3 downto 0);

   	------------------
   	-- Output ports --
   	------------------
   	Q : out std_logic_vector(3 downto 0)
    
    );
End entity Right_Shift;

Architecture Right_Shift_ARCH of Right_Shift is
	signal Q_buf : std_logic_vector(3 downto 0);
Begin
	process(clk)
	Begin
		if clk'event and clk = '1' then
			if clr = '1' then Q_buf <= "0000";
			elsif Ld = '1' then Q_buf <= D;
			elsif RS = '1' then Q_buf <= Lin & Q_buf(3 downto 1);
			end if;
		end if;
		Q <= Q_buf;
	end process;

End Architecture Right_Shift_ARCH;