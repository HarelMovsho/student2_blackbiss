--______ _    _    _____ _____  ___  ___  ___
--|  ___| |  | |  |_   _|  ___|/ _ \ |  \/  |
--| |_  | |  | |    | | | |__ / /_\ \| .  . |
--|  _| | |/\| |    | | |  __||  _  || |\/| |
--| |   \  /\  /    | | | |___| | | || |  | |
--\_|    \/  \/     \_/ \____/\_| |_/\_|  |_/
--
--------------------------------------------------------------------------------
-- Projetc: ‏‏Student2_Chapter17_1_13
-- File: ‏‏Student2_Chapter17_1_13.vhd
--------------------------------------------------------------------------------
-- Description: the answer to the question.
--
--------------------------------------------------------------------------------
--       Version:       Author:          Date:       Change:
--            0.1       Student2         13/4/2020
--
--
--------------------------------------------------------------------------------
Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
-- TODO: import lib here, if needed

Entity Priority_Encoder is
   port(
   	-------------------
   	-- General Ports --
   	-------------------
   	clk	: in std_logic;
   	rst : in std_logic;

   	-----------------
   	-- Input ports --
   	-----------------
   	I0 : in bit;
   	I1 : in bit;
   	I2 : in bit;
      I3 : in bit;

   	------------------
   	-- Output ports --
   	------------------
      V : out bit;
   	O0 : out bit;
      O1 : out bit
    
    );
End entity Priority_Encoder;

Architecture Priority_Encoder_ARCH of Priority_Encoder is
Begin
	process(I3, I2, I1, I0)
   Begin
      if I3 = '1' then
         V <= '1';
         O0 <= '1';
         O1 <= '1';
      elsif I2 = '1' then
         V <= '1';
         O0 <= '0';
         O1 <= '1';
      elsif I1 = '1' then
         V <= '1';
         O0 <= '1';
         O1 <= '0';
      elsif I0 = '1' then
         V <= '1';
         O0 <= '0';
         O1 <= '0';
      else V <= '0';
      End if;
   End process;

End Architecture Priority_Encoder_ARCH;